/*
 * 2021 Collegiate eCTF
 * SCEWL Bus Controller implementation
 * Ben Janis
 *
 * (c) 2021 The MITRE Corporation
 *
 * This source file is part of an example system for MITRE's 2021 Embedded System CTF (eCTF).
 * This code is being provided only for educational purposes for the 2021 MITRE eCTF competition,
 * and may not meet MITRE standards for quality. Use this code at your own risk!
 */

#include "controller.h"
#include <wolfssl/wolfcrypt/types.h>
#include <wolfssl/wolfcrypt/aes.h>

#define send_str(M) send_msg(RAD_INTF, SCEWL_ID, SCEWL_FAA_ID, strlen(M), M)
#define BLOCK_SIZE 16

// message, crypto-data, and crypto-auth buffer
char buf_m[SCEWL_MAX_DATA_SZ];
char buf_c[SCEWL_MAX_DATA_SZ];

// key buffer and IV buffer
char buf_aes_key[SCEWL_AES_KEY_SZ];
char buf_aes_iv[SCEWL_AES_IV_SZ];

// global store for the highest valid seq number we've seen
int high_seq = 0;

//TODO key rotation
//this could operate on a timer interrupt w/ RTC. if this is the case,
//we must treat aes_decrypt/aes_encrypt as a critical section. that is,
//    if we are encrypting, we want to service the interrupt, rotate keys,
//      then restart the encryption with the proper key
//    if we are decrypting, we want to finish decryption with the stale key,
//      then service the interrupt
// this will lead to some complex behavior based on the states of our entire 
// SED network, and is worth putting much thought into as a team
int rotate_key(){
  buf_aes_key[0] = '1';
}

// int aes_decrypt
//  given a cipher text (data_ct), and a buffer for a plaintext,
//  decrypt the ct into the pt buffer using the current global key  
int aes_decrypt(char* data_ct, char* data_pt, len)
{
  Aes aes;
  // set the key using the current global key
  if(wc_AesSetKey(&aes, buf_aes_key, SCEWL_AES_KEY_SZ, buf_aes_iv, AES_DECRYPTION))
  {
    return AES_SET_KEY_FAILURE;
  }
  // run an integrity check first
  //TODO verify the MAC
  //make sure seq_num > last_seq
  // run the decryption
  //TODO using wc_AesGcmDecrypt 
  //*note, we have to account for the fact that data is structured like:
  //   uint32_t  seq_num; 
  //   char[NONCE_LEN] encryption_nonce; 
  //   char[RAW_LEN] encrypted_body;
  //   char[MAC_LEN] packet_mac;
  // 
  //development notes of how this function can respond:
  //return MSG_INTEGRITY_FAILURE
  //return MSG_SEQ_INVALID
  //return AES_ENC_FAILURE
  //return AES_DEC_FAILURE;
  return AES_SUCCESS;
}

// int aes_encrypt
//  given a plaintext (data_pt), and a buffer for a ciphertext,
//  encrypt the pt into the ct buffer using the current global key
int aes_encrypt(char* data_pt, char* data_ct, len)
{
  Aes aes;
  //set the key using the current global key
  if(wc_AesSetKey(&aes, buf_aes_key, SCEWL_AES_KEY_SZ, buf_aes_iv, AES_DECRYPTION))
  {
    return AES_SET_KEY_FAILURE;
  }

  // run the encryption
  //TODO using wc_AesGcmEncrypt
  //*note, we have to account for the fact that data is structured like:
  //   uint32_t  seq_num; 
  //   char[NONCE_LEN] encryption_nonce; 
  //   char[RAW_LEN] encrypted_body;
  //   char[MAC_LEN] packet_mac;
  return AES_SUCCESS;
}

// int read_msg
//  read data from network interfaces. 
//  TODO*developer notes: we care a good deal about
//  what goes on in this function. this happens before the processing
//  stage where we verify & decrypt, so we should have checks for safe input
int read_msg(intf_t *intf, char *data, scewl_id_t *src_id, scewl_id_t *tgt_id,
             size_t n, int blocking) 
{
  scewl_hdr_t hdr;
  int read, max;
  
  // clear buffer and header
  memset(&hdr, 0, sizeof(hdr));
  memset(data, 0, n);
  
  //find header start
  do 
  {
    hdr.magicC = 0;

    if (intf_read(intf, (char *)&hdr.magicC, 1, blocking) == INTF_NO_DATA) 
    {
      return SCEWL_NO_MSG;
    }

    if (hdr.magicS == 'S') 
    {
      do 
      {
        if (intf_read(intf, (char *)&hdr.magicC, 1, blocking) == INTF_NO_DATA) 
        {
          return SCEWL_NO_MSG;
        }
      } while (hdr.magicC == 'S'); // in case of multiple 'S's in a row
    }
  } while (hdr.magicS != 'S' && hdr.magicC != 'C');

  // read rest of header
  read = intf_read(intf, (char *)&hdr + 2, sizeof(scewl_hdr_t) - 2, blocking);
  if(read == INTF_NO_DATA) 
  {
    return SCEWL_NO_MSG;
  }
   
  // unpack header
  *src_id = hdr.src_id;
  *tgt_id = hdr.tgt_id;

  // read body
  max = hdr.len < n ? hdr.len : n;
  read = intf_read(intf, data, max, blocking);

  // throw away rest of message if too long
  for (int i = 0; hdr.len > max && i < hdr.len - max; i++) 
  {
    intf_readb(intf, 0);
  }

  // report if not blocking and full message not received
  if(read == INTF_NO_DATA || read < max) 
  {
    return SCEWL_NO_MSG;
  }

  return max;
}

// int send_msg
//  data in buffer (char *data) should be ready to send upon entering this function.
//  that is: if sending, data is encypted. 
//           if recieving (sending to cpu), data is decrypted
//  additionally, time-blocking should be complete 
int send_msg(intf_t *intf, scewl_id_t src_id, scewl_id_t tgt_id, uint16_t len, char *data) 
{
  scewl_hdr_t hdr;

  // pack header
  hdr.magicS  = 'S';
  hdr.magicC  = 'C';
  hdr.src_id = src_id;
  hdr.tgt_id = tgt_id;
  hdr.len    = len;

  // send header
  intf_write(intf, (char *)&hdr, sizeof(scewl_hdr_t));

  // send body
  intf_write(intf, data, len);

  return SCEWL_OK;
}

// int handle_scewl_recv
//  handle recieving messages from SEDs. we recieve encrypted data, decrypt here,
//  verify the integrity of the packet, then pass it through to the CPU.
// *developer notes: after processing, data_pt should contain the RAW BODY
//  OF THE PLAINTEXT WE WISH TO PASS THROUGH. This means dropping the rest
//  of the scewl message meta data.
int handle_scewl_recv(char* data_ct, char* data_pt, scewl_id_t src_id, uint16_t len) 
{
  // initial sanity checks here
  //  ...
  //  ...

  //TODO make sure we are ready to decrypt.
  // that is, ensure we are not decrypting
  // too rapidly. there are many ways to do
  // this. for example, we could wait here
  // for some time to pass, or we could wait
  // after each time we decrypt to make sure
  // each processing takes exactly as much 
  // as we are allowed. we are given these
  // time contraints (in seconds):
  // TIME_TGT_MSG_SEND (the time we have to send a targeted message)
  // TIME_TGT_MSG_RECV (the time we have to recieve a targeted message)
  // TIME_BCS_MSG_SEND (the time we have to send a broadcast message)
  // TIME_BCS_MSG_RECV (the time we have to recieve a broadcast message)

  // run the integrity check & decryption
  int ret = aes_decrypt(data_ct, data_pt, len);
  
  if(ret == AES_SUCCESS)
  {
    //we did well!
    return send_msg(CPU_INTF, src_id, SCEWL_ID, len, data_pt);
  }

  // oops, this isn't good code to be in.
  // are we under attack? let us check
  // the return code to see what happened   
  if(ret == MSG_INTEGRITY_FAILURE)
    return 0 //TODO handle this accordingly

  if(ret == MSG_SEQ_INVALID)
    return 0 //TODO handle this accordingly

  if(ret == AES_SET_KEY_FAILURE)
    return 0 //TODO handle this accordingly
  
  if(ret == AES_DEC_FAILURE)
    return 0 //TODO handle this accordingly

  // OOPS, we REALLY don't want to be ~here~!
  return -1;
}

// int handle_scewl_send
//  handle sending messages to SEDs. we recieve plaintext data from
//  the CPU, encrypt it here, then pass back the encrypted packet to
//  the CPU.
//  *developer notes: after processing, data_ct should contain the entire
//  packet body to be sent, excluding the SCEWL header
int handle_scewl_send(char* data_pt, char* data_ct, scewl_id_t tgt_id, uint16_t len) 
{
  return send_msg(RAD_INTF, SCEWL_ID, tgt_id, len, data);
}

// int handle_brdcst_recv
//  TODO this will mimic the behavior of scewl_recv, so let's finish that first
int handle_brdcst_recv(char* data_ct, char* data_pt, scewl_id_t src_id, uint16_t len) 
{
  return send_msg(CPU_INTF, src_id, SCEWL_BRDCST_ID, len, data);
}

// int handle_brdcst_send
//  TODO this will mimic the behaviour of scewl_send, so let's finish that first
int handle_brdcst_send(char* data_pt, char* data_ct uint16_t len) 
{
  return send_msg(RAD_INTF, SCEWL_ID, SCEWL_BRDCST_ID, len, data);
}   


int handle_faa_recv(char* data, uint16_t len) 
{
  return send_msg(CPU_INTF, SCEWL_FAA_ID, SCEWL_ID, len, data);
}


int handle_faa_send(char* data, uint16_t len) 
{
  return send_msg(RAD_INTF, SCEWL_ID, SCEWL_FAA_ID, len, data);
}


int handle_registration(char* msg) 
{
  scewl_sss_msg_t *sss_msg = (scewl_sss_msg_t *)msg;
  if (sss_msg->op == SCEWL_SSS_REG) 
  {
    return sss_register();
  }
  else if (sss_msg->op == SCEWL_SSS_DEREG) 
  {
    return sss_deregister();
  }

  // bad op
  return 0;
}


int sss_register() 
{
  scewl_sss_msg_t msg;
  scewl_id_t src_id, tgt_id;
  int status, len;

  // fill registration message
  msg.dev_id = SCEWL_ID;
  msg.op = SCEWL_SSS_REG;
  
  // send registration
  status = send_msg(SSS_INTF, SCEWL_ID, SCEWL_SSS_ID, sizeof(msg), (char *)&msg);
  if (status == SCEWL_ERR) 
  {
    return 0;
  }

  // receive response
  len = read_msg(SSS_INTF, (char *)&msg, &src_id, &tgt_id, sizeof(scewl_sss_msg_t), 1);

  // notify CPU of response
  status = send_msg(CPU_INTF, src_id, tgt_id, len, (char *)&msg);
  if (status == SCEWL_ERR) 
  {
    return 0;
  }

  // op should be REG on success
  return msg.op == SCEWL_SSS_REG;
}


int sss_deregister() 
{
  scewl_sss_msg_t msg;
  scewl_id_t src_id, tgt_id;
  int status, len;

  // fill registration message
  msg.dev_id = SCEWL_ID;
  msg.op = SCEWL_SSS_DEREG;
  
  // send registration
  status = send_msg(SSS_INTF, SCEWL_ID, SCEWL_SSS_ID, sizeof(msg), (char *)&msg);
  if (status == SCEWL_ERR) 
  {
    return 0;
  }

  // receive response
  len = read_msg(SSS_INTF, (char *)&msg, &src_id, &tgt_id, sizeof(scewl_sss_msg_t), 1);

  // notify CPU of response
  status = send_msg(CPU_INTF, src_id, tgt_id, len, (char *)&msg);
  if (status == SCEWL_ERR) 
  {
    return 0;
  }

  // op should be DEREG on success
  return msg.op == SCEWL_SSS_DEREG;
}

int main() {
  int registered = 0, len;
  scewl_hdr_t hdr;
  uint16_t src_id, tgt_id;

  // initialize interfaces
  intf_init(CPU_INTF);
  intf_init(SSS_INTF);
  intf_init(RAD_INTF);

  // clear out the message buffers
  memset(&buf_m, 0, SCEWL_MAX_DATA_SZ);
  memset(&buf_c, 0, SCEWL_MAX_DATA_SZ);

  //serve forever
  while (1) 
  {
    // register with SSS
    read_msg(CPU_INTF, buf_m, &hdr.src_id, &hdr.tgt_id, sizeof(buf_m), 1);

    if (hdr.tgt_id == SCEWL_SSS_ID) 
    {
      registered = handle_registration(buf_m);
    }

    // server while registered
    while (registered) 
    {
      memset(&hdr, 0, sizeof(hdr));

      // handle outgoing message from CPU
      if (intf_avail(CPU_INTF)) 
      {
        // Read message from CPU
        len = read_msg(CPU_INTF, buf_m, &src_id, &tgt_id, sizeof(buf_m), 1);

        if (tgt_id == SCEWL_BRDCST_ID) 
        {
          handle_brdcst_send(buf_m, buf_c, len);
        } 
        else if (tgt_id == SCEWL_SSS_ID) 
        {
          registered = handle_registration(buf_m);
        } 
        else if (tgt_id == SCEWL_FAA_ID) 
        {
          handle_faa_send(buf_m, len);
        }
        else 
        {
          handle_scewl_send(buf_m, buf_c, tgt_id, len);
        }

        continue;
      }

      // handle incoming radio message
      if (intf_avail(RAD_INTF)) 
      {
        // Read message from antenna
        len = read_msg(RAD_INTF, buf_m, &src_id, &tgt_id, sizeof(buf_m), 1);
       
        if (src_id != SCEWL_ID) 
        { //ignore our own outgoing messages
          if (tgt_id == SCEWL_BRDCST_ID) 
          {
            // receive broadcast message
            handle_brdcst_recv(buf_m, buf_c, src_id, len);
          } 
          else if (tgt_id == SCEWL_ID) 
          {
            // receive unicast message
            if (src_id == SCEWL_FAA_ID) 
            {
              handle_faa_recv(buf_m, len);
            } 
            else 
            {
              handle_scewl_recv(buf_m, buf_c, src_id, len);
            }
          }
        }
      }
    }
  }
}
